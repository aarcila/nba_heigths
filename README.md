# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version 3.0.0 Rails '6.1.4'

* To run the code 

# Rails Installation on Linux

We are installing Ruby On Rails on Linux using rbenv. It is a lightweight Ruby Version Management Tool. The rbenv provides an easy installation procedure to manage various versions of Ruby, and a solid environment for developing Ruby on Rails applications.
Follow the steps given below to install Ruby on Rails using rbenv tool.

### Step 1: Install Prerequisite Dependencies
First of all, we have to install git - core and some ruby dependences that help to install Ruby on Rails. Use the following command for installing Rails dependencies using yum.

```
tp> sudo yum install -y git-core zlib zlib-devel gcc-c++ patch readline readline-devel libyaml-devel libffi-devel openssl-devel make bzip2 autoconf automake libtool bison curl sqlite-devel
```
### Step 2: Install rbenv
Now we will install rbenv and set the appropriate environment variables. Use the following set of commands to get rbenv for git repository.
```
tp> git clone git://github.com/sstephenson/rbenv.git .rbenv
tp> echo 'export PATH = "$HOME/.rbenv/bin:$PATH"' >> ~/.bash_profile
tp> echo 'eval "$(rbenv init -)"' >> ~/.bash_profile
tp> exec $SHELL

tp> git clone git://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
tp> echo 'export PATH = "$HOME/.rbenv/plugins/ruby-build/bin:$PATH"' << ~/.bash_profile
tp> exec $SHELL
```
### Step 3: Install Ruby
Before installing Ruby, determine which version of Ruby you want to install. We will install Ruby 3.0.0. Use the following command for installing Ruby.
```
tp> rbenv install -v 3.0.0
```
Use the following command for setting up the current Ruby version as default.
```
tp> rbenv global 3.0.0
```
Use the following command to verify the Ruby version.
```
tp> ruby -v
```
**Output**
```
ruby 3.0.0p173
```
Ruby provides a keyword gem for installing the supported dependencies; we call them gems. If you don't want to install the documentation for Ruby-gems, then use the following command.
```
tp> echo "gem: --no-document" > ~/.gemrc
```
Thereafter, it is better to install the Bundler gem, because it helps to manage your application dependencies. Use the following command to install bundler gem.
```
tp> gem install bundler
```
###Step 4: Install Rails
Use the following command for installing Rails version 6.1.4.
```
tp> install rails -v 6.1.4
```
Use the following command to make Rails executable available.
```
tp> rbenv rehash
```
Use the following command for checking the rails version.
```
tp> rails -v
```
***Output***
```
tp> Rails 6.1.4
```
Ruby on Rails framework requires JavaScript Runtime Environment (Node.js) to manage the features of Rails. Next, we will see how we can use Node.js to manage Asset Pipeline which is a Rails feature.

Documentation from https://www.tutorialspoint.com/ruby-on-rails/rails-installation.htmø