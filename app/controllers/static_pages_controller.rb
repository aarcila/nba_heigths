# frozen_string_literal: true

require 'net/http'
require 'json'

class StaticPagesController < ApplicationController
  before_action :load_json

  def index; end

  def search
    if params[:search].blank?
      redirect_to(root_path, alert: "Empty field!") and return
    else
      @parameter = params[:search].downcase
      @res.each do |value|
        player = []
        value[1].each do |val|
          player << val if val['h_in'].downcase.include? @parameter
        end
        @result = player
      end
    end
  end

  private

  def load_json
    url = 'https://mach-eight.uc.r.appspot.com'
    uri = URI(url)
    response = Net::HTTP.get(uri)
    @res = JSON.parse(response)
  end
end